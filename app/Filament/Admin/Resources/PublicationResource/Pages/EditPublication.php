<?php

namespace App\Filament\Admin\Resources\PublicationResource\Pages;

use App\Filament\Admin\Resources\PublicationResource;
use Filament\Resources\Pages\EditRecord;

class EditPublication extends EditRecord
{
    protected static string $resource = PublicationResource::class;

    protected function getHeaderActions(): array
    {
        return [

        ];
    }
}
