<?php

namespace App\Filament\Admin\Widgets;

use Filament\Widgets\ChartWidget;

class Downloads extends ChartWidget
{
    protected static ?string $heading = 'Downloads';

    protected function getData(): array
    {
        return [
            //
        ];
    }

    protected function getType(): string
    {
        return 'bar';
    }
}
