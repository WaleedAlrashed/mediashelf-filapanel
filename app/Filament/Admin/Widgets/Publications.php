<?php

namespace App\Filament\Admin\Widgets;

use Filament\Widgets\ChartWidget;

class Publications extends ChartWidget
{
    protected static ?string $heading = 'Publications';

    protected function getData(): array
    {
        return [
            //
        ];
    }

    protected function getType(): string
    {
        return 'line';
    }
}
