<?php

namespace App\Filament\Admin\Widgets;

use Filament\Widgets\ChartWidget;

class Authors extends ChartWidget
{
    protected static ?string $heading = 'Authors';

    protected function getData(): array
    {
        return [
            //
        ];
    }

    protected function getType(): string
    {
        return 'line';
    }
}
